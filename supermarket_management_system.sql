-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 08, 2019 at 05:09 PM
-- Server version: 5.7.26-0ubuntu0.19.04.1
-- PHP Version: 7.2.19-0ubuntu0.19.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `supermarket_management_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `city_name`) VALUES
(1, 'Allahabad'),
(2, 'Varanasi');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `country_id` int(11) NOT NULL,
  `country_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `country_name`) VALUES
(1, 'India'),
(2, 'USA');

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `order_id` int(11) NOT NULL,
  `order_customer_name` varchar(255) NOT NULL,
  `order_customer_mobile` varchar(255) NOT NULL,
  `order_total` varchar(255) NOT NULL,
  `order_status` varchar(255) NOT NULL,
  `order_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`order_id`, `order_customer_name`, `order_customer_mobile`, `order_total`, `order_status`, `order_date`) VALUES
(3, 'Kaushal Kishor', '9876543212', '21.5', 'Paid', 'Tuesday 12th of February 2019 01:08:37 PM'),
(4, 'Atul Kumar', '9878675645', '76', 'Paid', 'Tuesday 12th of February 2019 02:17:32 PM'),
(5, 'Kaushal', '94385034', '44', 'Paid', 'Tuesday 12th of February 2019 02:39:35 PM'),
(9, 'Rahul Kumar', '987654323', '112.5', 'Paid', 'Tuesday 12th of February 2019 04:40:32 PM'),
(10, 'Kaushal', '8978675645', '600', 'Paid', 'Tuesday 12th of February 2019 04:47:37 PM'),
(11, 'Dolly', '8978675645', '210', 'Paid', 'Tuesday 12th of February 2019 04:50:33 PM'),
(13, 'Kaushal', '493573', '39', 'Paid', 'Tuesday 11th of June 2019 10:52:57 AM'),
(14, 'Anil', '9876543212', '348.5', 'Paid', 'Tuesday 11th of June 2019 11:42:34 AM'),
(15, 'fghjkl', 'f', '150', 'Paid', 'Monday 24th of June 2019 02:11:46 PM'),
(16, 'Ram Kapoor', '8470010001', '330', 'Paid', 'Monday 24th of June 2019 02:22:37 PM'),
(17, 'Ram Kapoor', '8470010001', '0', 'Paid', 'Thursday 27th of June 2019 03:41:16 PM'),
(18, 'Ram Kapoor', '8470010001', '0', 'In Progress', 'Friday 28th of June 2019 09:38:26 AM'),
(19, 'Ram Kapoor', '8470010001', '330', 'Paid', 'Friday 28th of June 2019 09:41:07 AM'),
(20, 'Ram Kapoor', '8470010001', '330', 'Paid', 'Monday 8th of July 2019 05:00:53 PM');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `product_code` varchar(255) NOT NULL,
  `product_stock` int(11) NOT NULL,
  `product_type_id` varchar(255) NOT NULL,
  `product_title` varchar(255) NOT NULL,
  `product_price_per_item` varchar(255) NOT NULL,
  `product_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_code`, `product_stock`, `product_type_id`, `product_title`, `product_price_per_item`, `product_description`) VALUES
(1, 'M10001', 79, '6', 'Good Day Biscuits', '250', 'Good Day Biscuits'),
(2, 'M10002', 28, '2', 'Gram Flour', '150', 'Gram Flour'),
(3, 'M10003', 306, '1', 'Tur Pulse', '150', 'Tur Pulse'),
(5, 'M10004', 2, '5', 'Black Paper', '80', 'Black Paper'),
(6, 'M10005', 1, '1', 'Red Rajma', '100', 'Red Rajma'),
(7, 'M10006', 10, '3', 'Sella Rice', '120', 'Sella Rice'),
(8, 'M10007', 8, '5', 'Turmeric Powder', '100', 'Turmeric Powder'),
(9, 'M10008', 9, '4', 'Tata Tea', '100', 'Tata Tea'),
(10, 'M10009', 0, '7', 'Haldiram Namkeen', '100', 'Haldiram Namkeen');

-- --------------------------------------------------------

--
-- Table structure for table `sell`
--

CREATE TABLE `sell` (
  `sell_id` int(11) NOT NULL,
  `sell_order_id` varchar(255) NOT NULL,
  `sell_product_id` varchar(255) NOT NULL,
  `sell_units` varchar(255) NOT NULL,
  `sell_price_per_unit` varchar(255) NOT NULL,
  `sell_total_cost` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sell`
--

INSERT INTO `sell` (`sell_id`, `sell_order_id`, `sell_product_id`, `sell_units`, `sell_price_per_unit`, `sell_total_cost`) VALUES
(7, '2', '1', '10', '200', '200'),
(12, '2', '2', '1', '111', '111'),
(13, '2', '1', '1', '15', '150'),
(14, '2', '2', '1', '150', '150'),
(15, '4', '1', '2', '2', '4'),
(16, '4', '2', '11', '150', '16.5'),
(17, '4', '3', '37', '150', '55.5'),
(18, '5', '1', '19', '200', '38'),
(19, '5', '2', '4', '150', '6'),
(20, '6', '3', '20', '150', '30'),
(21, '7', '1', '1', '2', '2'),
(22, '7', '', '1', '', ''),
(26, '8', '6', '1', '10', '10'),
(27, '8', '1', '1', '2', '2'),
(28, '8', '5', '1', '1', '1'),
(29, '8', '', '1', '', ''),
(30, '8', '2', '1', '1.50', '1.50'),
(34, '9', '2', '1', '1.50', '1.50'),
(35, '9', '5', '1', '1', '1'),
(36, '9', '6', '1', '10', '10'),
(37, '9', '8', '1', '100', '100'),
(38, '10', '10', '6', '100', '600'),
(40, '11', '10', '1', '100', '100'),
(41, '11', '8', '1', '100', '100'),
(42, '11', '6', '1', '10', '10'),
(43, '12', '', '1', '', ''),
(47, '12', '9', '2', '10', '20'),
(48, '12', '6', '1', '10', '10'),
(49, '13', '3', '4', '1.50', '6'),
(50, '13', '6', '3', '10', '30'),
(51, '13', '2', '2', '1.50', '3'),
(52, '14', '9', '1', '10', '10'),
(53, '14', '5', '4', '1', '4'),
(54, '14', '6', '3', '10', '30'),
(55, '14', '2', '3', '1.50', '4.5'),
(56, '14', '10', '3', '100', '300'),
(57, '15', '2', '1', '150', '150'),
(61, '16', '2', '1', '150', '150'),
(62, '16', '6', '1', '100', '100'),
(63, '16', '5', '1', '80', '80'),
(64, '19', '5', '1', '80', '80'),
(65, '19', '1', '1', '250', '250'),
(66, '20', '5', '1', '80', '80'),
(67, '20', '1', '1', '250', '250');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `state_id` int(11) NOT NULL,
  `state_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`state_id`, `state_name`) VALUES
(1, 'UttarnPradesh'),
(2, 'Madhya Pradesh');

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE `type` (
  `type_id` int(11) NOT NULL,
  `type_name` varchar(255) NOT NULL,
  `type_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`type_id`, `type_name`, `type_description`) VALUES
(1, 'Pulses', 'Pulses'),
(2, 'Oil', 'Oil'),
(3, 'Rice', 'Rice'),
(4, 'Tea', 'Tea'),
(5, 'Garam Masala', 'Garam Masala'),
(6, 'Biscuits', 'Biscuits'),
(7, 'Namkeen', 'Sugar');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_level_id` varchar(255) NOT NULL DEFAULT '2',
  `user_username` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_add1` varchar(255) NOT NULL,
  `user_add2` varchar(255) NOT NULL,
  `user_city` varchar(255) NOT NULL,
  `user_state` varchar(255) NOT NULL,
  `user_country` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_mobile` varchar(255) NOT NULL,
  `user_gender` varchar(255) NOT NULL,
  `user_dob` varchar(255) NOT NULL,
  `user_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_level_id`, `user_username`, `user_password`, `user_name`, `user_add1`, `user_add2`, `user_city`, `user_state`, `user_country`, `user_email`, `user_mobile`, `user_gender`, `user_dob`, `user_image`) VALUES
(7, '2', 'faculty', 'test', 'Amit Kumar', 'House no : 768', 'Sector 2B', '2', '1', '2', 'amit@gmail.com', '9324324546', '', '26 December,2015', 'IMG_5746.JPG'),
(8, '2', 'suman', 'test', 'Suman Singh', 'House no : 768', 'Sector 2A', '1', '2', '1', 'suman@gmail.com', '987654321', '', '13 January,1961', 'IMG_5748.jpg'),
(10, '3', 'manasa', 'test', 'Manasa', 'New Delhi', 'India', '2', '2', '1', 'manasa@gmail.com', '9876543212', '', '18 January,1968', 'IMG_5746.jpg'),
(16, '1', 'admin', 'test', 'Kaushal Kishore', 'House No : 355, Sector 23', 'Sector 2A', '1', '1', '1', 'kaushal.rahuljaiswal@gmail.com', '9567654565', '', '10 March,2016', 'Image.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `sell`
--
ALTER TABLE `sell`
  ADD PRIMARY KEY (`sell_id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `sell`
--
ALTER TABLE `sell`
  MODIFY `sell_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `state_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `type`
--
ALTER TABLE `type`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
